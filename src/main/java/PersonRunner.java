import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonRunner {

    public static void filterAndPrintByAgeAndHeight (List<Person> personList) {
        personList.stream()
                .filter(person -> person.getAge() > 20 || person.getHeight() > 1.75F)
                .forEach(person -> System.out.println(person.getName() + " " + person.getSurname()));

    }

    public static Map<String, Integer> convertToMap(List<Person> personList) {
        return personList.stream().collect(Collectors.toMap(person -> person.getName() + " " +person.getSurname(),person -> person.getAge()));
    }

    public static List<List<Integer>> filterNumbers (List<List<Integer>> lists) {
        return lists.stream()
                .filter(l -> l.contains(5))
                .collect(Collectors.toList());
    }


    public static void main(String[] args) {
        Person person = new Person("Robert", "Rossa 1", 28, 1.76f, "dupa");
        Person person2 = new Person("Wacław", "Rossa 2", 19, 1.76f, "dupa");
        Person person3 = new Person("Ziutek", "Rossa 3", 28, 1.56f, "dupa");
        Person person4 = new Person("Robert", "Rossa 4", 19, 1.56f, "dupa");
        List<Person> persons = new ArrayList<>();
        persons.add(person);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);


        filterAndPrintByAgeAndHeight(persons);


        System.out.println(convertToMap(persons).toString());


        System.out.println("LISTA LIST");
        List<Integer> list1 = Arrays.asList(2,5);
        List<Integer> list2 = Arrays.asList(3,4);
        List<Integer> list3 = Arrays.asList(3,4,5);
        List<List<Integer>> listOfList = Arrays.asList(list1, list2, list3);
        System.out.println(filterNumbers(listOfList));

    }
}
