import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StringEXC {



    public static List<String> stringChecker (List<String> stringList) {


     return stringList.stream()                         //mamy listę stringów - zamieniamy ją na streama (stram działa na konkretnym typie)
             .filter(dowolnaNazwaParametruPoKtorymFiltrujemy -> dowolnaNazwaParametruPoKtorymFiltrujemy.length() > 3)     //filtrujemy. interfejs funkcyjny. przyjmujemy string, zwracamy boolean
             .collect(Collectors.toList());             //zbieramy
    }

    public static long countStartingWithA (List<String> stringA) {
        return stringA.stream()
                .filter(cosik -> cosik.startsWith("A") && cosik.endsWith("A"))
                .count();

    }


    public static void main(String[] args) {

        stringChecker(Arrays.asList("cat","dog","kamil", "sda", "computer")).forEach(s -> System.out.println(s + " "));
        System.out.println();
        stringChecker(Arrays.asList("one","two","three", "four", "five")).forEach(s -> System.out.println(s + " "));
        System.out.println();
        stringChecker(Arrays.asList("aa","aaa","aaaa", "bb", "bbbb")).forEach(s -> System.out.println(s + " "));
        System.out.println();
        System.out.println(countStartingWithA(Arrays.asList("Ala", "ApA", "MałpA", "AsiA", "ALE")));
    }

}


